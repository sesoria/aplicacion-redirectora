import socket
import random
import sys

# Create a TCP objet socket and bind it to a port
# We bind to 'localhost', therefore only accepts connections from the
# same machine
# Port should be 80, but since it needs root privileges,
# let's use one above 1024

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.bind(('localhost', int(sys.argv[1])))

# Queue a maximum of 5 TCP connection requests

mySocket.listen(5)

# Accept connections, read incoming data, and answer back an HTLM page
#  (in a loop)

while True:
    print("Waiting for connections")
    (recvSocket, address) = mySocket.accept()
    print("HTTP request received:")
    received = recvSocket.recv(2048)
    print(received)

    lista_urls =["http://gsyc.es", "https://www.3djuegos.com", "https://www.apple.com/es", "https://www.balenciaga.com/es-es", "https://www.orange.es"]
    aleatorio = random.randint(0,len(lista_urls))
    print(lista_urls[aleatorio-1])

    response = "HTTP/1.1 301 Moved Permanently\r\n" \
                + f"Location: {lista_urls[aleatorio-1]}\r\n\r\n"
    recvSocket.send(response.encode('utf-8'))
    recvSocket.close()
